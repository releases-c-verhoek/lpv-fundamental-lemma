%% ===================================================================== %%
%  Numerical example used in Section V.C of the 2021 CDC paper:
%    C. Verhoek, R. Tóth, S. Haesaert, and A. Koch. "Fundamental Lemma for 
%     Data-Driven Analysis of Linear Parameter-Varying Systems." In Proc.
%     of the 60th IEEE Conference on Decision and Control (CDC), 2021, 
%     pp. 5040-5046.
%
%  The code present a simulation example using the SISO LPV system that has
%  been presented in [1]. We use Lemma 2 from the paper to simulate the 
%  system for L = 30 steps, given an initial trajectory (util, ptil, ytil)
%  of length Tini, the future input and scheduling trajectories (ubar,pbar)
%  of length L and a data-dictionary (u,p,y) of persistently exciting data.
%
%
%  Final version -- Results used in published paper.
%  Tested in ML2020b, ML2021b on both mac and windows machine
%  
% [1] C. Verhoek, H. S. Abbas, R. Tóth, and S. Haesaert. "Data-Driven
%   Predictive Control for Linear Parameter-Varying Systems." In Proc. of
%   the 4th IFAC Workshop on Linear Parameter-Varying Systems (LPVS), 2021.
%
%
%   Author: Chris Verhoek
%
%  Released under the BSD 3-Clause License. 
%  Copyright (c) 2022, C. Verhoek. Eindhoven, The Netherlands.
%% ===================================================================== %%

clear variables; close all; clc;
rng(23);
dims = {};
%% Parameters
L = 30;     % length of the prediction
Tini = 2;   % length of initial trajectory
Nd = 193;   % number of data-points

np = 2;     % scheduling dimension
nu = 1;     % number of inputs
ny = 1;     % number of outputs
nx = 2;     % number of states

%% simulate full system
% make dimension struct
dims.nx = nx;
dims.nu = nu;
dims.ny = ny;
dims.np = np;
dims.Nd = Nd;
dims.L  = L;
dims.Tini = Tini;


% Trajectories for simulation
ud = 4*(rand(Nd,nu)-0.5);
u2 = 4*(rand((Tini+L),nu)-0.5);
pd = [[0;conv(rand(round(Nd/2),1),0.1*sin((1:Nd/2)*2*pi*0.07)')],...
          [0;conv(rand(round(Nd/2),1),0.1*(cos((1:Nd/2)*2*pi*0.17)'))]];
p2 = [[conv(rand(round((Tini+L)/2),1),0.1*sin((1:(Tini+L)/2)*2*pi*0.07)');0],...
          [0;conv(rand(round((Tini+L)/2),1),0.1*(cos((1:(Tini+L)/2)*2*pi*0.17)'))]];


%% simulate full system
y0 = 0;                                         % initial y
yd = simsystem(pd, ud, Nd, dims.ny, y0);        % data dictionary
y2 = simsystem(p2, u2, Tini+L, dims.ny, y0);    % initial trajectory

%% preprocessing
% make processing structs
data = {};
data.u = ud;
data.p = pd;
data.y = yd;

data.pred.u = u2;
data.pred.p = p2;
data.pred.y = y2;

data.dims = dims;

%% Data-driven problem -- Simulation scenario
signals = getStaticAffineData(data);

yp = solveDataDrivenProblem(signals);

Y2_pred_nan = [NaN(1,Tini),yp];

%% Plots
clm = lines(5);
% Data dictionary
% input
fd1=figure(11); clf; fd1.Position(3:4) = [400 200];grid on; box on; hold on;
stairs(1:Nd,ud,'LineWidth',1, 'Color',clm(1,:),'DisplayName',...
'Input signal $u_d(k)$'); ylim([-2.2,2.2]); set(gca,'YTick',[-2.1:0.7:2.1])
legend('show');xlim([1 Nd]);xlabel('Time-step $k$'); 
title(['Data-dictionary: Input -- $N_\mathrm{d}$=',num2str(Nd)]);
try fd1=tightfig(fd1);catch;disp('no tightfig func, no problem'); end
% scheduling
fd2=figure(12); clf; fd2.Position(3:4) = [400 200];grid on; box on; hold on;
stairs(1:Nd,pd(:,1),'LineWidth',1, 'Color',clm(4,:),'DisplayName','Scheduling signal $p_{d,1}(k)$')
stairs(1:Nd,pd(:,2),'LineWidth',1, 'Color',clm(5,:),'DisplayName','Scheduling signal $p_{d,2}(k)$')
legend('show');xlim([1 Nd]);xlabel('Time-step $k$'); 
title(['Data-dictionary: Scheduling signal -- $N_\mathrm{d}$=',num2str(Nd)]);
try fd2=tightfig(fd2);catch;disp('no tightfig func, no problem'); end
% output
fd3=figure(13); clf; fd3.Position(3:4) = [400 200];grid on; box on; hold on;
stairs(1:Nd,yd,'LineWidth',1, 'Color',clm(2,:),'DisplayName','Output $y_d(k)$')
legend('show');xlim([1 Nd]);xlabel('Time-step $k$'); 
title(['Data-dictionary: Output -- $N_\mathrm{d}$=',num2str(Nd)]);
try fd3=tightfig(fd3);catch;disp('no tightfig func, no problem'); end

% Simulation data
% input
fs1=figure(21); clf; fs1.Position(3:4) = [400 200];grid on; box on; hold on;
stairs(1:Tini+1,[signals.u_ini,signals.u_fut(1)],'LineWidth',2, 'Color',...
   clm(1,:),'DisplayName','Initial input trajectory $u_{\mathrm{ini}}(k)$')
stairs(1+Tini:L+Tini,signals.u_fut,'-.','LineWidth',2, 'Color',clm(1,:),...
   'DisplayName','Future input trajectory $u_{\mathrm{fut}}(k)$')
legend('show');xlim([1 L+Tini]);xlabel('Time-step $k$'); 
title(sprintf('Simulation data: Input -- $T_{\\mathrm{ini}}=%i, L=%i$',Tini,L));
ylim([-2.2,2.2]); set(gca,'YTick', [-2.1:0.7:2.1])
% scheduling
fs2=figure(22); clf; fs2.Position(3:4) = [400 200];grid on; box on; hold on;
stairs(1:Tini+1,[signals.p_ini(1,:),signals.p_fut(1,1)],'LineWidth',2,'Color',...
  clm(4,:),'DisplayName','Initial scheduling trajectory $p_{\mathrm{ini},1}(k)$')
stairs(1+Tini:L+Tini,signals.p_fut(1,:),'-.','LineWidth',2, 'Color',...
  clm(4,:),'DisplayName','Future scheduling trajectory $p_{\mathrm{fut},1}(k)$')
stairs(1:Tini+1,[signals.p_ini(2,:),signals.p_fut(2,1)],'LineWidth',2,'Color',...
  clm(5,:),'DisplayName','Initial scheduling trajectory $p_{\mathrm{ini},2}(k)$')
stairs(1+Tini:L+Tini,signals.p_fut(2,:),'-.','LineWidth',2,'Color',clm(5,:),...
  'DisplayName','Future scheduling trajectory $p_{\mathrm{fut},2}(k)$')
legend('show');xlim([1 L+Tini]);xlabel('Time-step $k$'); 
title(sprintf('Simulation data: Scheduling variables -- $T_{\\mathrm{ini}}=%i, L=%i$',Tini,L));
% output 
fs3=figure(23); clf; fs3.Position(3:4) = [400 200];grid on; box on; hold on;
stairs(1:Tini+1,[signals.y_ini,signals.y_fut(1)],'LineWidth',2, 'Color',...
  clm(2,:),'DisplayName','Initial output trajectory $y_{\mathrm{ini}}(k)$')
stairs(1+Tini:L+Tini,signals.y_fut,'-.','LineWidth',2, 'Color',clm(2,:),...
    'DisplayName','Future output trajectory $y_{\mathrm{fut}}(k)$')
legend('show');xlim([1 L+Tini]);xlabel('Time-step $k$'); 
title(sprintf('Simulation data: Output -- $T_{\\mathrm{ini}}=%i, L=%i$',Tini,L));
try fs1=tightfig(fs1);catch;disp('no tightfig func, no problem'); end
try fs2=tightfig(fs2);catch;disp('no tightfig func, no problem'); end
try fs3=tightfig(fs3);catch;disp('no tightfig func, no problem'); end

% Plot result
f=figure(1); clf; f.Position(3:4) = [916 270];
hold on; grid on; box on;
stairs(1:Tini+L,y2,'Color',0.8*ones(1,3),'LineWidth',5,...
  'DisplayName','True simulated output $y(k)$')
stairs(1:Tini+1,[signals.y_ini,signals.y_fut(1)],'--','Color',clm(1,:),...
  'LineWidth',2.5,'DisplayName','Initial output trajectory $\tilde{y}(k)$')
stairs(1+Tini:Tini+L,yp,'--','Color',clm(4,:),'LineWidth',2.5,...
  'DisplayName','Predicted output trajectory $\bar{y}(k)$')
legend('show','FontSize',15, 'Orientation','horizontal');xlim([1 Tini+L+1])
ylim([-1.22,1.42]); xticks([1:4:32, 32]); set(gca,'FontSize',14);
xlabel('Time-step $k$','FontSize',16);
try f=tightfig(f);catch;disp('no tightfig func, no problem'); end

%% ##################################################################### %%
%  #####################################################################  %
%  ########################## LOCAL FUNCTIONS ##########################  %
%  #####################################################################  %
%  #####################################################################  %

%% Simulate system
function y = simsystem(p, u, T, ny, y0)
    % model parameters
    a10 = 1.0; a11 = -0.5; a12 = -0.1;
    a20 = 0.5; a21 = -0.7; a22 = -0.1;
    b10 = 0.5; b11 = -0.4; b12 = 0.01;
    b20 = 0.2; b21 = -0.3; b22 = -0.2;
    y = zeros(ny,T);
    for k =1:T
        if k == 1
            y(k) = y0;
        elseif k == 2
            a1p = a10+a11*p(k-1,1)+a12*p(k-1,2);
            b1p = b10+b11*p(k-1,1)+b12*p(k-1,2);
            y(k) = -a1p*y(k-1)+b1p*u(k-1);
        else
            a1p = a10+a11*p(k-1,1)+a12*p(k-1,2);
            a2p = a20+a21*p(k-2,1)+a22*p(k-2,2);
            b1p = b10+b11*p(k-1,1)+b12*p(k-1,2);
            b2p = b20+b21*p(k-2,1)+b22*p(k-2,2);
            y(k) = -a1p*y(k-1)-a2p*y(k-2)+b1p*u(k-1)+b2p*u(k-2);
        end
    end
end

%% Get the static affine standard data
function signals = getStaticAffineData(data)
    % input: Data struct
    nu   = data.dims.nu;
    ny   = data.dims.ny;
    np   = data.dims.np;
    Nd   = data.dims.Nd;
    L    = data.dims.L;
    Tini = data.dims.Tini;

    %% check correct dimensions
    % output
    if (size(data.y,1) == ny) && (size(data.y,2) == Nd);     y = data.y;
    elseif (size(data.y,2) == ny) && (size(data.y,1) == Nd); y = data.y';
    else; error('output data incorrect size'); end
    % input
    if (size(data.u,1) == nu) && (size(data.u,2) == Nd);        u = data.u;
    elseif (size(data.u,2) == nu) && (size(data.u,1) == Nd);    u = data.u';
    else;     error('input data incorrect size'); end
    % scheduling
    if (size(data.p,1) == np) && (size(data.p,2) == Nd);        p = data.p;
    elseif (size(data.p,2) == np) && (size(data.p,1) == Nd);    p = data.p';
    else;     error('scheduling data incorrect size'); end
    % prediction data
    % input initial
    if (size(data.pred.u,2) == nu) && (size(data.pred.u,1) == (Tini+L)); data.pred.u = data.pred.u';
    else; assert((size(data.pred.u,1) == nu) && (size(data.pred.u,2) == (Tini+L)),'initial input data incorrect size'); end
    % scheduling initial
    if (size(data.pred.p,2) == np) && (size(data.pred.p,1) == (Tini+L)); data.pred.p = data.pred.p';
    else; assert((size(data.pred.p,1) == np) && (size(data.pred.p,2) == (Tini+L)),'initial scheduling data incorrect size'); end
    % output initial
    if (size(data.pred.y,2) == ny) && (size(data.pred.y,1) == (Tini+L)); data.pred.y = data.pred.y';
    else; assert((size(data.pred.y,1) == ny) && (size(data.pred.y,2) == (Tini+L)),'initial output data incorrect size'); end
    %% Create data dictionary
    % make u^p, y^p signals
    signals.up = reshape(u'.*reshape(p',Nd,1,[]),Nd,[])';
    signals.yp = reshape(y'.*reshape(p',Nd,1,[]),Nd,[])';
    signals.y = y;
    signals.u = u;
    signals.p = p;
    signals.dims = data.dims;
    signals.original_data = data;
    %% Prediction data
    % split initial data and future data
    signals.u_ini = data.pred.u(:,1:Tini);
    signals.p_ini = data.pred.p(:,1:Tini);
    signals.y_ini = data.pred.y(:,1:Tini);
    signals.u_fut = data.pred.u(:,1+Tini:Tini+L);
    signals.p_fut = data.pred.p(:,1+Tini:Tini+L);
    signals.y_fut = data.pred.y(:,1+Tini:Tini+L);
end

%% Solve the data-driven problem
function predicted_output = solveDataDrivenProblem(signals, showerror)
% Input: Signals struct
    if nargin < 2; showerror = false; end
    % Extract signals struct
    up    = signals.up;
    yp    = signals.yp;
    y     = signals.y;
    u     = signals.u;
    u_ini = signals.u_ini;
    p_ini = signals.p_ini;
    y_ini = signals.y_ini;
    u_fut = signals.u_fut;
    p_fut = signals.p_fut;
    y_fut = signals.y_fut;
    nu    = signals.dims.nu;
    ny    = signals.dims.ny;
    np    = signals.dims.np;
    L     = signals.dims.L;
    Tini  = signals.dims.Tini;

    %% Data dictionary
    Hddu  =  makeHankel(u,Tini+L);
    Hddup =  makeHankel(up,Tini+L);
    Hddy  =  makeHankel(y,Tini+L);
    Hddyp =  makeHankel(yp,Tini+L);

    Hddu_tini  = Hddu(1:nu*Tini,:);
    Hddup_tini = Hddup(1:np*nu*Tini,:);
    Hddy_tini  = Hddy(1:ny*Tini,:);
    Hddyp_tini = Hddyp(1:np*ny*Tini,:);

    Hddu_L  = Hddu(nu*Tini+1:(Tini+L)*nu,:);
    Hddup_L = Hddup(np*nu*Tini+1:(Tini+L)*np*nu,:);
    Hddy_L  = Hddy(ny*Tini+1:(Tini+L)*ny,:);
    Hddyp_L = Hddyp(np*ny*Tini+1:(Tini+L)*np*ny,:);

    % Pbar^n matrice
    Pini_nu = createPbarn(p_ini,nu);
    Pini_ny = createPbarn(p_ini,ny);
    PL_nu   = createPbarn(p_fut,nu);
    PL_ny   = createPbarn(p_fut,ny);

    % Left-hand side of representation
    LHS = [Hddu_tini;
           Hddup_tini-Pini_nu*Hddu_tini;
           Hddy_tini;
           Hddyp_tini-Pini_ny*Hddy_tini;
           Hddu_L;
           Hddup_L-PL_nu*Hddu_L;
           Hddyp_L-PL_ny*Hddy_L];
    % Right-hand side of representation
    RHS = [reshape(u_ini,[],1);
           zeros(np*nu*Tini,1);
           reshape(y_ini,[],1);
           zeros(np*ny*Tini,1);
           reshape(u_fut,[],1);
           zeros(np*nu*L,1);
           zeros(np*ny*L,1)];

    % solve problem and give predicted output
    g = LHS\RHS;
    ypred_col = Hddy_L*g;
    predicted_output = reshape(ypred_col,ny,[]);

    if showerror
        error = max(max(abs(y_fut-predicted_output)));
        fprintf('Maximum error in prediction: %d\n',error);
    end 
end

%% Make Hankel Matrix
function H = makeHankel(x,L)
% H = makeHankel(x,L)
% Determines the hankel matrix for a dataset $\{x_i\}_{i=1}^N$, where
% $x_i\in\mathbb{R}^n$. L is the length of the trajectory we consider.
% Hence, the hankel matrix will have the form
%
%      [  x1   x2   ..  x_N-L+1 ]
%      [  x2   x3   ..  x_N-L+2 ]
%  H = [  :    :     \ \   :    ]
%      [ x_L  x_L+1 ...   x_N   ]
    [n,N] = size(x);
    Hs = hankel(1:L,L:N);
    Hr = reshape(Hs,1,[]);
    xr = x(:,Hr);
    H = reshape(xr, n*L,[]);
end

%% Make the \bar{\mathcal{P}}^{n} matrix
function Pbarn = createPbarn(p,n)
% p is of dimension np x Nd
    Pbarn = [];
    nd = size(p,2);
    for ii = 1:nd
        Pbarn = blkdiag(Pbarn,kron(p(:,ii),eye(n)));
    end
end
